
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Alexis
 */
public class ConvertisseurGraphique {

    private JFrame frameObj;
    private JTextField taux, montant;
    private JLabel resultat;

    /**
     * Construit l'interface graphique en associant à celle-ci un controleur
     *
     * @param cont
     */
    public ConvertisseurGraphique(Controleur cont) {
        GridLayout grid = new GridLayout(4, 2);

        this.frameObj = new JFrame();
        this.frameObj.setTitle("Convertisseur");

        this.frameObj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frameObj.setSize(275, 150);

        JPanel panel = new JPanel();
        this.frameObj.setContentPane(panel);
        panel.setLayout(grid);

        JLabel label1 = new JLabel("Taux de conversion");
        JLabel label2 = new JLabel("Somme à convertir");
        JLabel label3 = new JLabel("Resultat");
        this.resultat = new JLabel("");
        JLabel label5 = new JLabel("");

        this.taux = new JTextField();
        this.montant = new JTextField();

        JButton btn1 = new JButton("Convertir");
        btn1.addActionListener(cont);

        //Permet de quitter avec la touche echap
        KeyListener kl = new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                //If someone click Esc key, this program will exit
                if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    System.exit(0);
                }
            }
        };

        //add Key Listener to JFrame
        frameObj.addKeyListener(kl);

        panel.add(label1);
        panel.add(this.taux);
        panel.add(label2);
        panel.add(this.montant);
        panel.add(label3);
        panel.add(this.resultat);
        panel.add(label5);
        panel.add(btn1);
    }

    /**
     * Retourne le taux saisi par l'utilisateur
     *
     * @return le taux à convertir
     * @throws java.lang.NumberFormatException
     */
    public float donneTaux() throws java.lang.NumberFormatException {

        try {
            return Float.parseFloat(this.taux.getText());
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid float in argumment");
            return 0;
        }
    }

    /**
     * Retourne le montant saisi par l'utilisateur
     *
     * @return le montant à convertir
     * @throws java.lang.NumberFormatException
     */
    public float donneMontant() throws java.lang.NumberFormatException {

        try {
            return Float.parseFloat(this.montant.getText());
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid float in argumment");
            return 0;
        }
    }

    /**
     * Affiche le resultat de la conversion
     *
     * @param resultat le resultat de la conversion
     */
    public void afficheResultat(float resultat) {
        this.resultat.setText(String.valueOf(resultat));
    }

    /**
     * Affiche une boite de dialogue avec un message d'erreur dans le cas ou une
     * erreur s'est produite durant la conversion.
     *
     * @param ex l'exception signalant le problème
     */
    public void afficheErreur(java.lang.NumberFormatException ex) {
        System.err.println(ex.getMessage());
    }

    /**
     * Affiche l'interface graphique une fois que les references entre les trois
     * objets (interface, controleur, convertisseur) ont ete initialisees.
     */
    public void afficheInterface() {
        this.frameObj.pack();
        this.frameObj.setVisible(true);
    }
}
