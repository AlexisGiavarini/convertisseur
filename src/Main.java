/**
 * Represente la classe principale de l'application qui contient la methode
 * <code>main</code>.
 */
public class Main
{
    /**
     * Lance l'application en creant l'interface graphique, le controleur
     * et le convertisseur. Puis affiche l'interface graphique une fois que
     * les references entre les trois objets (interface, controleur,
     * convertisseur) ont ete initialisees.
     */
    public static void main(String[] args)
    {
        Convertisseur convertisseur = new Convertisseur();
        Controleur controleur = new Controleur(convertisseur);
        ConvertisseurGraphique interfaceGraphique = new ConvertisseurGraphique(controleur);
        controleur.associeInterfaceGraphique(interfaceGraphique);
        // a appeler en dernier de preference
        // (une fois que tous les objets ont ete crees)...
        interfaceGraphique.afficheInterface();
    }

}
