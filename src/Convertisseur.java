/**
 *
 * @author Alexis
 */
public class Convertisseur {
    
    public Convertisseur(){}
    
    /**
     * Effectue la conversion d'une somme à partir d'un taux
     * @param nombre
     * @param taux
     * @return la somme convertie
     */
    public float conversion(float nombre, float taux){
        return nombre * taux;
    }
}
