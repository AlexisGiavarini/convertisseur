
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author Alexis
 */
public class Controleur implements ActionListener{ 
    private Convertisseur convertisseur;
    private ConvertisseurGraphique cg;
    
    /**
     * Construit un controleur en lui fournissant une reference au convertisseur
     * @param conv le convertisseur à associer
     */
    public Controleur(Convertisseur conv){
        this.convertisseur = conv;
    }
    
    /**
     * Action executée lors de l'activation du bouton "Convertir"
     * Effectue une conversion en recuperant les informations (taux et montant) 
     * aupres de l'interface graphique puis en appelant la methode conversion du
     * convertisseur.
     * @param e evenement généré par l'activation du bouton
     */
    public void actionPerformed(ActionEvent e){
        float taux = cg.donneTaux();
        float montant = cg.donneMontant();

        if(taux  > 0 && montant > 0){
           this.cg.afficheResultat(this.convertisseur.conversion(taux, montant));
        }else{
            NumberFormatException nfe = new NumberFormatException("Nombre strictement positifs uniquement");
            this.cg.afficheErreur(nfe);
        }  
    }
    
    /**
     * Associe une interface graphique au controleur.
     * @param cg le convertisseur graphique
     */
    public void associeInterfaceGraphique(ConvertisseurGraphique cg){
        this.cg = cg;
    }   
}
