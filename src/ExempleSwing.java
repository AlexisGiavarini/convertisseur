import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Action;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/** Cette classe est d�riv�e de AbstractAction repr�sentant une action
 * pouvant �tre d�clench�e par l'interface utilisateur via diverses
 * sources d'�v�nements (menu, widget, clavier). Une action est un objet :
 * - une description de commande (cha�ne de texte et une ic�ne facultative)
 * - les param�tres n�cessaires � l'ex�cution de la commande (dans ce
 *   cas la couleur d�sir�e).
 */
class ColorAction extends AbstractAction
{
   // composant cible de l'action
   private Component target;
   
   public ColorAction(String name, Icon icon, Color c, Component comp)
   {
      // chaine de texte associ�e � la commande
      putValue(Action.NAME, name);
      // (petite) icone associ�e � la commande
      putValue(Action.SMALL_ICON, icon);
      // couleur d�sir�e
      putValue("Color",c);
      // composant dont la couleur doit changer
      target = comp;    
   }

   /** Permet de modifier la couleur de fond */
   public void actionPerformed(ActionEvent evt)
   {
      // recuperation de la valeur de la nouvelle couleur 
      Color c = (Color) getValue("Color");
      // on modifie la couleur de fond de la cible
      target.setBackground(c);
      // on force le rafraichissement
      target.repaint();
   }
}

/** Cette classe permet d'associer un bouton � une action */
class ActionButton extends JButton
{
   public ActionButton(Action a)
   {
      // r�cup�ration du libell� du bouton
      setText((String) a.getValue(Action.NAME));
      // r�cup�ration de l'icone associ�e � l'action
      Icon icon = (Icon) a.getValue(Action.SMALL_ICON);
      if (icon != null) 
      {
         setIcon(icon);
      }
      // association de l'action au bouton, les evts du bouton iront
      // directement � l'objet action associ�...
      addActionListener(a);
   }
}

class ExempleSwingFrame extends JFrame
{
   public ExempleSwingFrame()
   {
      setTitle("ExempleSwing");
      setSize(300,200);
      // permet de sortir de l'application lors de la fermeture de la
      // fen�tre...
      addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e)
            {
               System.exit(0);
            }
         });
      
      // panneau sur lequel vont �tre plac�s les boutons...
      JPanel panel = new JPanel();
      
      // creation des trois boutons
      Action blueAction = new ColorAction("Blue", new
                                          ImageIcon("blue-ball.gif"),
                                          Color.blue, panel);
      Action yellowAction = new ColorAction("Yellow", new
                                            ImageIcon("yellow-ball.gif"),
                                          Color.yellow, panel);
      Action redAction = new ColorAction("Red", new
                                         ImageIcon("red-ball.gif"),
                                          Color.red, panel);
      // ajout des boutons au panneau
      panel.add(new ActionButton(blueAction));
      panel.add(new ActionButton(yellowAction));
      panel.add(new ActionButton(redAction));

      // permet � l'application de percevoir les frappes de touches
      // un objet KeyStroke repr�sente une touche du clavier
      // codage touche : VK_ (Virtual Keyboard) + lettre associ�e
      // le param�tre WHEN_IN_FOCUSED_WINDOW signifie : prendre en
      // compte la frappe si elle a lieu n'importe o� ds la fen�tre
      panel.registerKeyboardAction(blueAction,
                                   KeyStroke.getKeyStroke(KeyEvent.VK_B, 0),
                                   JComponent.WHEN_IN_FOCUSED_WINDOW);
      panel.registerKeyboardAction(yellowAction,
                                   KeyStroke.getKeyStroke(KeyEvent.VK_Y, 0),
                                   JComponent.WHEN_IN_FOCUSED_WINDOW);
      panel.registerKeyboardAction(redAction,
                                   KeyStroke.getKeyStroke(KeyEvent.VK_R, 0),
                                   JComponent.WHEN_IN_FOCUSED_WINDOW);
      // ajout du panneau � la fen�tre
      Container contentPane = getContentPane();
      contentPane.add(panel);

      // cr�ation du menu
      JMenu m = new JMenu("Color");
      // ajout des items
      m.add(blueAction);
      m.add(yellowAction);
      m.add(redAction);
      // cr�ation de la barre de menu
      JMenuBar mbar = new JMenuBar();
      // ajout du menu
      mbar.add(m);
      // ajout de la barre de menu a la fen�tre principale
      setJMenuBar(mbar);
   }
}

/** Test des classes pr�c�dentes **/
public class ExempleSwing 
{
   public static void main (String[] args) 
   {
      JFrame frame = new ExempleSwingFrame();
      frame.show();
   }
}// ExempleSwing
